<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index', ['url'=>'', 'submitted'=>false]);
});

Route::post('/', function (Illuminate\Http\Request $request) {
	$goose = new Goose\Client();
	$article = $goose->extractContent($request->url);
	$response = [
		'url' => $request->url,
		'submitted' => true,
		'language' => $article->getLanguage(),
		'publishDate' => is_null($article->getPublishDate()) ? '' : $article->getPublishDate()->format('Y-m-d H:i:s'),
		'title' => $article->getTitle(),
		'metaDescription' => $article->getMetaDescription(),
		'metaKeywords' => $article->getMetaKeywords(),
		'canonicalLink' => $article->getCanonicalLink(),
		'finalUrl' => $article->getFinalUrl(),
		'openGraph' => collect($article->getOpenGraph())->toJson(),
		'domain' => $article->getDomain(),
		'tags' => collect($article->getTags())->toJson(),
		'links' => collect($article->getLinks())->toJson(),
		'videos' => collect($article->getVideos())->toJson(),
		'articleText' => $article->getCleanedArticleText(),
		'entities' => collect($article->getPopularWords())->toJson(),
		'image' => is_null($article->getTopImage()) ? '' : $article->getTopImage()->getImageSrc(),
		'allImages' => collect($article->getAllImages())->map(function ($item) {return $item->getImageSrc();})->toJson(),
		// 'doc' => $article->getDoc(),
		// 'additionalData' => $article->getAdditionalData(),
		// 'openGraphData' => $article->getOpenGraphData(),
		'popularWords' => collect($article->getPopularWords())->toJson(),
	];
	// dd($response);
	return view('index', $response);
});

/*
{!--
        <tr><th>publishDate</th><td>{{ $publishDate }}</td></tr>,
    'openGraph' => $article->getOpenGraph(),
    'tags' => $article->getTags(),
    'links' => $article->getLinks(),
    'videos' => $article->getVideos(),
    'entities' => $article->getPopularWords(),
    'image' => $article->getTopImage(),
    'allImages' => $article->getAllImages(),
    'doc' => $article->getDoc(),
    'additionalData' => $article->getAdditionalData(),
    'openGraphData' => $article->getOpenGraphData(),
    'popularWords' => $article->getPopularWords(),
--}