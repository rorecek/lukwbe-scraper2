
<!DOCTYPE html>
<html lang="en" class="route-documentation">
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">

  <title>PHP-Goose Test</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.4.2/css/bulma.min.css">

</head>

  <body class="">
<nav class="nav">
  <div class="container">
  <div class="nav-left">
    <a class="nav-item">
      <strong>Web Scrapper</strong>
    </a>
  </div>

  <!-- This "nav-toggle" hamburger menu is only visible on mobile -->
  <!-- You need JavaScript to toggle the "is-active" class on "nav-menu" -->
  <span class="nav-toggle">
    <span></span>
    <span></span>
    <span></span>
  </span>

  <!-- This "nav-menu" is hidden on mobile -->
  <!-- Add the modifier "is-active" to display it on mobile -->
  <div class="nav-right nav-menu">
    <a class="nav-item" href="/">
      Home
    </a>
  </div>
</nav>



  <section class="hero is-medium is-info is-bold">
    <div class="hero-body">
      <div class="container">
        
        <div class="columns">
          <div class="column is-10 is-offset-1">
            <form method="post" action="/">
              {{ csrf_field() }}
              <div class="field has-addons">
                <p class="control has-icons-left is-expanded">
                  <input class="input is-success" type="text" name="url" placeholder="Enter URL for web scraper" value="{{ $url }}">
                  <span class="icon is-small is-left">
                    <i class="fa fa-external-link"></i>
                  </span>
                </p>
                <p class="control">
                  <button class="button is-primary">Submit</button>
                </p>
              </div>
            </form>
          </div>
        </div>

      </div>
    </div>
  </section>

@if ($submitted)
  <div class="container">
    <h2 class="title is-2"><br />Results</h2>
    <table class="table">
      <tbody>
        <tr><th>language</th><td>{{ $language }}</td></tr>
        <tr><th>publishDate</th><td>{{ $publishDate }}</td></tr>
        <tr><th>title</th><td>{{ $title }}</td></tr>
        <tr><th>metaDescription</th><td>{{ $metaDescription }}</td></tr>
        <tr><th>metaKeywords</th><td>{{ $metaKeywords }}</td></tr>
        <tr><th>canonicalLink</th><td>{{ $canonicalLink }}</td></tr>
        <tr><th>finalUrl</th><td>{{ $finalUrl }}</td></tr>
        <tr><th>openGraph</th><td>{{ $openGraph }}</td></tr>
        <tr><th>domain</th><td>{{ $domain }}</td></tr>
        <tr><th>articleText</th><td>{!! nl2br($articleText) !!}</td></tr>
        <tr><th>image</th><td>{{ $image }}</td></tr>
        <tr><th>allImages</th><td>{{ $allImages }}</td></tr>
        <tr><th>tags</th><td>{{ $tags }}</td></tr>
        <tr><th>links</th><td>{{ $links }}</td></tr>
        <tr><th>videos</th><td>{{ $videos }}</td></tr>
        <tr><th>popularWords</th><td>{{ $popularWords }}</td></tr>
      </tbody>
    </table>
  </div>
@endif
  </body>
</html>
